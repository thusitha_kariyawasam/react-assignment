# Assignment

## Development Scripts

### Install dependencies
To install all the needed dependencies, use:
```
npm install
```

### Start server
To start the static server, use:
```
npm start
```
*Go to http://localhost:3000 to view page.*

### Tests
To perform all the tests (using flowtype and jest), use:
```
npm test
```

### Builds
To build the code (using Webpack) into a single javascript file, use:
```
npm run build
```