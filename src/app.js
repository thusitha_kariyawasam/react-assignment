/**
 * Created by thusitha on 11/28/18.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import Route from './router';
import configureStore from './store/configureStore';

const store = configureStore();

import './styles/main.scss';

ReactDOM.render(
    <Provider store={store}>
        <Route/>
    </Provider>,
    window.document.getElementById('app')
);

if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
        module.hot.accept();
    }
}
