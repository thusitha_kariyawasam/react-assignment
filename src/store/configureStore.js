/**
 * Created by thusitha on 11/28/18.
 */
'use strict';

import { createStore, applyMiddleware } from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import rootReducer from '../reducers';

export default function configureStore() {
    const middleware = process.env.NODE_ENV !== 'production'
        ? [reduxImmutableStateInvariant(), thunk, createLogger()]
        : [thunk];

    const composeEnhancers = composeWithDevTools({});
    return createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(...middleware)),
    );
}
