/**
 * Created by thusitha on 11/28/18.
 */
'use strict';
import {Observable} from 'rxjs';

const ajax = Observable.ajax;

const HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};


export function get(url, headers) {
    headers = Object.assign({}, HEADERS, headers);
    return new Promise((resolve, reject) => {
        ajax.get(url, headers).subscribe(response => {
            return resolve(response.response);
        }, err => {
            return reject({
                status: err.xhr.status,
                response: err.xhr.response
            });
        });
    });
}
