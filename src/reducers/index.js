/**
 * Created by thusitha on 11/28/18.
 */

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {transactionReducers} from '../transaction';

const rootReducer = combineReducers({
    routing: routerReducer,
    ...transactionReducers
});

export default rootReducer;
