/**
 * Created by thusitha on 11/28/18.
 */
import React from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import {TransactionMainWrapper} from './transaction';

const App = () => (
    <Router>
        <Switch>
            <Route exact={true} path="/" component={TransactionMainWrapper}/>
            <Redirect to="/"/>
        </Switch>
    </Router>
);

export default App;