/**
 * Created by thusitha on 11/28/18.
 */
import {get} from "../../helpers/request.helper";
import * as CONFIG from '../../../config/config.json';
import {transactionActions} from '../index';

export const getTransaction = () => dispatch => {
    return get(CONFIG.TRANSACTIONS, {}).then((data) => {
        dispatch(transactionActions.getTransactions(data))
    });
};