/**
 * Created by thusitha on 11/28/18.
 */

export const GET_TRANSACTIONS = 'GET_TRANSACTIONS';

export function getTransactions(payload) {
    return {type: GET_TRANSACTIONS, payload: payload};
}

export const ADD_TRANSACTIONS = 'ADD_TRANSACTIONS';

export function addTransactions(payload) {
    return {type: ADD_TRANSACTIONS, payload: payload};
}