/**
 * Created by thusitha on 11/29/18.
 */
import {createSelector} from 'reselect';
import * as _ from 'lodash';

const transactions = state => state['transaction']['data'] || [];


export const getLineData = createSelector([transactions], (transaction) => {

        let labels = [];
        let dataSets = [];
        let paymentMode = [];

        _.forOwn(_.groupBy(transaction, 'userName'), function (value, key) {
            labels.push(key)
        });

        _.forOwn(_.groupBy(transaction, 'paymentMode'), function (value, key) {
            paymentMode.push(key)
        });

        for (let mode in paymentMode) {
            let data = [0];
            for (let label in labels) {
                let amount = 0;
                for (let trans in transaction) {
                    let obj = transaction[trans];
                    if (obj.userName === labels[label] && obj.paymentMode === paymentMode[mode]) {
                        amount = amount + parseInt(obj.amount);
                    }
                }
                data.push(amount);
            }
            dataSets.push({label: paymentMode[mode], data: data});
        }

        return {labels: labels, dataSets: dataSets};
    }
);