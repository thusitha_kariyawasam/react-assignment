/**
 * Created by thusitha on 11/29/18.
 */

import {createSelector} from 'reselect';

const transactions = state => state['transaction']['data'] || [];
let labels = [];
let data = [];

export const getPieData = createSelector([transactions], (transaction) => {
        transaction.map(result => {
            const index = labels.indexOf(result.paymentMode);
            if (index === -1) {
                labels.push(result.paymentMode);
                data.push(parseInt(result.amount));
            } else {
                data[index] = data[index] + parseInt(result.amount);
            }
        });
        return {labels: labels, data: data};
    }
);