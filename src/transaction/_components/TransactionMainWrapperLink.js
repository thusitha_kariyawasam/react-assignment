/**
 * Created by thusitha on 11/28/18.
 */
import {connect} from 'react-redux';
import TransactionMainWrapper from './TransactionMainWrapper';
import {transactionThunks, transactionActions} from '../index';

function mapStateToProps(state) {
    return {
        transaction: state['transaction']['data'] || []
    };
}

const mapDispatchToProps = dispatch => {
    return {
        getTransaction: () => {
            dispatch(transactionThunks.getTransaction());
        },
        addTransaction: (payload) => {
            dispatch(transactionActions.addTransactions(payload));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionMainWrapper)