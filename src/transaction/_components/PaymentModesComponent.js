/**
 * Created by thusitha on 11/29/18.
 */

import React from 'react';
import PropTypes from 'prop-types';

class PaymentModesComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedOption: 'VISA'
        };
        this.handleOptionChange = this.handleOptionChange.bind(this);
    }

    /**
     * Handle Payment selection
     * @param changeEvent
     */

    handleOptionChange(changeEvent) {
        const value = changeEvent.target.value;
        this.setState({
            selectedOption: value
        });
        this.props.updatePaymentMode(value);
    };

    render() {
        return (
            <div>
                <form>
                <div className="radio">
                    <label>
                        <input type="radio" value="American Express"
                               checked={this.state.selectedOption === 'American Express'}
                               onChange={this.handleOptionChange} />
                        American Express
                    </label>
                </div>
                <div className="radio">
                    <label>
                        <input type="radio" value="VISA"
                               checked={this.state.selectedOption === 'VISA'}
                               onChange={this.handleOptionChange} />
                        VISA
                    </label>
                </div>
                <div className="radio">
                    <label>
                        <input type="radio" value="DBS PayLa"
                               checked={this.state.selectedOption === 'DBS PayLa'}
                               onChange={this.handleOptionChange} />
                        DBS PayLa
                    </label>
                </div>
            </form>
            </div>
        );
    }
}

export default PaymentModesComponent;