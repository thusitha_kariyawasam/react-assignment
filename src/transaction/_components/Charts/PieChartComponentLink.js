/**
 * Created by thusitha on 11/29/18.
 */

import {connect} from 'react-redux';
import PieChartComponent from './PieChartComponent';
import {getPieData} from '../../_selectors/pie.selector';

function mapStateToProps(state) {
    return {
        pieData: getPieData(state)
    };
}

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(PieChartComponent);