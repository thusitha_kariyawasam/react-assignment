/**
 * Created by thusitha on 11/29/18.
 */

import {connect} from 'react-redux';
import LineChartComponent from './LineChartComponent';
import {getLineData} from '../../_selectors/line.selector';

function mapStateToProps(state) {
    return {
        lineData: getLineData(state)
    };
}

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LineChartComponent);