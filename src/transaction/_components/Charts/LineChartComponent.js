/**
 * Created by thusitha on 11/29/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {Line} from 'react-chartjs-2';

class LineChartComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const lineData = this.props.lineData.labels && this.props.lineData.labels.length > 1 ? {
            labels: this.props.lineData.labels,
            datasets: [
                {
                    label: this.props.lineData.dataSets[0].label,
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.props.lineData.dataSets[0].data
                },
                {
                    label: this.props.lineData.dataSets[1].label,
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(168, 86, 0,0.4)',
                    borderColor: 'rgba(168, 86, 0,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(168, 86, 0,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(168, 86, 0,1)',
                    pointHoverBorderColor: 'rgba(168, 86, 0,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.props.lineData.dataSets[1].data
                },
                {
                    label: this.props.lineData.dataSets[2].label,
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(0, 168, 83,0.4)',
                    borderColor: 'rgba(0, 168, 83,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(0, 168, 83,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(0, 168, 83,1)',
                    pointHoverBorderColor: 'rgba(0, 168, 83,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.props.lineData.dataSets[2].data
                }
            ]
        } : {};

        return (
            <div className="line-chart">
                <label>Line Chart</label>
                {this.props.lineData.labels && this.props.lineData.labels.length > 1 && <Line data={lineData} key={Math.random()}/>}
            </div>
        );
    }
}

export default LineChartComponent;