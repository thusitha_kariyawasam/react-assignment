/**
 * Created by thusitha on 11/29/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {Pie} from 'react-chartjs-2';

class PieChartComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let chartJsPie = {
            labels: this.props.pieData.labels,
            datasets: [{
                data: this.props.pieData.data,
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ]
            }]
        };
        return (
            <div className="pie-chart">
                <label>Pie Chart</label>
                {this.props.pieData.data && this.props.pieData.data.length > 1 && <Pie data={chartJsPie} key={Math.random()}/>}
            </div>
        );
    }
}

export default PieChartComponent;