/**
 * Created by thusitha on 11/28/18.
 */

import React from 'react';
import PropTypes from 'prop-types';

class TableItems extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="table-item">
                <div className="row">
                    <div className="col-sm-3 col-md-3 col-lg-3">{this.props.transactionID}</div>
                    <div className="col-sm-3 col-md-3 col-lg-3">{this.props.userName}</div>
                    <div className="col-sm-3 col-md-3 col-lg-3">{this.props.paymentMode}</div>
                    <div className="col-sm-3 col-md-3 col-lg-3">{this.props.amount}</div>
                </div>
            </div>
        );
    }
}

class TransactionTableComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="transaction-table">
                <div className="row table-name">
                    <div className="col-sm-3 col-md-3 col-lg-3">Transaction ID</div>
                    <div className="col-sm-3 col-md-3 col-lg-3">User Name</div>
                    <div className="col-sm-3 col-md-3 col-lg-3">Payment Mode</div>
                    <div className="col-sm-3 col-md-3 col-lg-3">Amount</div>
                </div>
                {this.props.transaction.map(result => {
                    return <TableItems {...result} key={result.transactionID}/>
                })}
            </div>
        );
    }
}

export default TransactionTableComponent;