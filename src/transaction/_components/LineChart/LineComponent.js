/**
 * Created by thusitha on 11/28/18.
 */
import React from 'react';
import PropTypes from 'prop-types';

export default class LineComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let {path, stroke, fill, strokeWidth} = this.props;
        return (
            <path
                d={path}
                fill={fill}
                stroke={stroke}
                strokeWidth={strokeWidth}
            />
        );
    }
};

LineComponent.propTypes = {
    path: PropTypes.string.isRequired,
    stroke: PropTypes.string,
    fill: PropTypes.string,
    strokeWidth: PropTypes.number
};

LineComponent.defaultProps = {
    stroke: 'blue',
    fill: 'none',
    strokeWidth: 3
};
