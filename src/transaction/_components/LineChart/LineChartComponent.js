/**
 * Created by thusitha on 11/28/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import DataSeriesComponent from './DataSeriesComponent';

export default class LineChartComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let {width, height, data} = this.props;

        let xScale = d3.scale.ordinal()
            .domain(data.xValues)
            .rangePoints([0, width]);

        let yScale = d3.scale.linear()
            .range([height, 10])
            .domain([data.yMin, data.yMax]);

        return (
            <svg width={width} height={height}>
                <DataSeriesComponent
                    xScale={xScale}
                    yScale={yScale}
                    data={data}
                    width={width}
                    height={height}
                />
            </svg>
        );
    }
};

LineChartComponent.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    data: PropTypes.object.isRequired
};

LineChartComponent.defaultProps = {
    width: 600,
    height: 300
};