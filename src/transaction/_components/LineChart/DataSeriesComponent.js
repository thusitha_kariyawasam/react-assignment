/**
 * Created by thusitha on 11/28/18.
 */
import React from 'react';
import LineComponent from './LineComponent';
import PropTypes from 'prop-types';

export default class DataSeriesComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let {data, colors, xScale, yScale, interpolationType} = this.props;

        let line = d3.svg.line()
            .interpolate(interpolationType)
            .x((d) => {
                return xScale(d.x);
            })
            .y((d) => {
                return yScale(d.y);
            });

        let lines = data.points.map((series, id) => {
            return (
                <LineComponent
                    path={line(series)}
                    stroke={colors(id)}
                    key={id}
                />
            );
        });

        return (
            <g>
                <g>{lines}</g>
            </g>
        );
    }
};

DataSeriesComponent.propTypes = {
    colors: PropTypes.func,
    data: PropTypes.object,
    interpolationType: PropTypes.string,
    xScale: PropTypes.func,
    yScale: PropTypes.func
};

DataSeriesComponent.defaultProps = {
    data: [],
    interpolationType: 'cardinal',
    colors: d3.scale.category10()
};