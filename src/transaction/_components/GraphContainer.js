/**
 * Created by thusitha on 11/28/18.
 */

import React from 'react';
import PropTypes from 'prop-types';

import {PieChartComponent, LineChartComponent} from '../index';

class GraphContainer extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <PieChartComponent />
                <LineChartComponent/>
            </div>
        );
    }
}

GraphContainer.propTypes = {
    transaction: PropTypes.array.isRequired
};

export default GraphContainer;