/**
 * Created by thusitha on 11/29/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {PaymentModesComponent} from '../index';

class PaymentComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            paymentMode: 'VISA',
            userName: '',
            amount: ''
        };
        this.handlePaymentMode = this.handlePaymentMode.bind(this);
    }

    /**
     * Handle User name changes
     * @param event
     */
    handleUserChange(event) {
        this.setState({userName: event.target.value})
    }

    /**
     * Handle amount changes
     * @param event
     */
    handleAmountChange(event) {
        let value = event.target.value;
        if (value > 0)
            this.setState({amount: event.target.value});
        else
            alert("Amount should be a positive number");
    }

    /**
     * Transfer Amount
     */
    transfer() {
        if (!this.state.userName) {
            alert("User Name is require for make a transaction ");
        } else if (!this.state.amount) {
            alert("Amount is require for make a transaction ");
        } else {
            this.props.addTransaction({transactionID: Math.floor(1000 + Math.random() * 9000), ...this.state});
        }
    }

    handlePaymentMode(mode) {
        this.setState({paymentMode: mode});
    }

    render() {
        return (
            <div className="row payment-container">
                <div className="col-4">
                    <input type="text" name="User Name" value={this.state.userName}
                           placeholder="User Name"
                           onChange={this.handleUserChange.bind(this)}/>
                </div>
                <div className="col-4">
                    <PaymentModesComponent updatePaymentMode={this.handlePaymentMode}/>
                </div>
                <div className="col-4 transfer-section">
                    <div>
                        <input type="text" name="Amount" value={this.state.amount}
                               placeholder="Amount"
                               onChange={this.handleAmountChange.bind(this)}/>

                        <button type="button" className="btn btn-info" onClick={this.transfer.bind(this)}>
                            Transfer
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default PaymentComponent;