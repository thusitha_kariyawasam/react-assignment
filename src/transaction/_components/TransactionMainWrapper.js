/**
 * Created by thusitha on 11/28/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import {TransactionTableComponent, GraphContainer, PaymentComponent} from '../index';

class TransactionMainWrapper extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getTransaction();
    }

    render() {
        return (
            <div>
                <PaymentComponent addTransaction={this.props.addTransaction}/>
                <TransactionTableComponent transaction={this.props.transaction}/>
                <GraphContainer transaction={this.props.transaction}/>
            </div>
        );
    }
}

TransactionMainWrapper.propTypes = {
    transaction: PropTypes.array.isRequired,
    addTransaction: PropTypes.func.isRequired
};

export default TransactionMainWrapper;