/**
 * Created by thusitha on 11/28/18.
 */

import {transactionActions} from '../index';
import * as _ from 'lodash';

export const INITIAL_STATE = {
    data: []
};

export function transactionReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case (transactionActions.GET_TRANSACTIONS): {
            return {
                ...state,
                data: action.payload
            }
        }
        case (transactionActions.ADD_TRANSACTIONS): {

            let data = _.cloneDeep(state.data);
            data.push(action.payload);
  
            return {
                ...state,
                data: data
            }
        }
        default: {
            return state;
        }
    }

}