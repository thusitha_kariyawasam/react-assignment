const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
/**
 * Created by thusitha on 11/28/18.
 */
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = env => ({
    entry: {
        app: ['babel-polyfill', 'whatwg-fetch', './src/app.js']
    },
    output: {
        path: `${__dirname}/dist`,
        filename: '[name].js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        'sass-loader'
                    ]
                })
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.eot$|\.woff$|\.woff2$|\.ttf$/,
                loader: 'url-loader',
                options: {
                    limit: '25000',
                    name: '[name].[hash:5].[ext]'
                }
            }
        ],
        noParse: [/moment.js/]
    },
    resolve: {
        modules: ['node_modules'],
        extensions: ['.js', '.jsx']
    },
    devServer: {
        port: 3000,
        historyApiFallback: true,
        hot: true
    },
    target: 'web',

    plugins: [
        new CleanWebpackPlugin('./dist', {
            verbose: env === 'production',
            dry: env === 'development'
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            hash: env === 'production'
        }),
        new ExtractTextPlugin({
            filename: '[name].css',
            allChunks: true,
            disable: env === 'development'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: ({ context }) => context && context.includes('node_modules')
        }),
        new webpack.optimize.UglifyJsPlugin({
            output: { comments: false },
            mangle: {
                except: ['super', '$', 'exports', 'require']
            }
        })
    ]
});
